from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todolist(request):
    todolist_list = TodoList.objects.all()
    context = {"todos": todolist_list}
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {"list_detail": details}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_list", id=todo.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("show_todo", id=id)
    else:
        form = TodoListForm(instance=todo)
    context = {"todo": todo, "form": form}
    return render(request, "todos/update.html", context)


def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create-item.html", context)


def update_todoitem(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("show_todo", id=id)
    else:
        form = TodoItemForm(instance=todo)
    context = {"todo": todo, "form": form}
    return render(request, "todos/update-item.html", context)
